package com.example.openfiredemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.jivesoftware.smack.util.StringUtils.isEmpty;

public class ReceivedActivity extends AppCompatActivity implements ConnectionListener {
    private static final String TAG = "ReceivedActivity";
    EditText etMessage;
   ImageView sendMsg;
    String senderId;
    String receiverId;
    String userPassword;
    String userName;
    XMPPTCPConnection mConnection;
    RecyclerView mRecyclerView;
    messageAdapter messageAdapterList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        sendMsg = findViewById(R.id.sendMsg);
        etMessage=findViewById(R.id.msg);
        mRecyclerView=findViewById(R.id.RecyclerView_Received);
//        connectUser();
//        sendMessage();


        Intent intent = getIntent();
        userName = intent.getStringExtra("user");
        userPassword = intent.getStringExtra("pass");
        Log.d(TAG, "userPassword: " + userPassword);
        Log.d(TAG, "userName : " + userName);
        String user1 = "testuser";
        String user2 = "testuser2";

        if (userName.equals("testuser")) {
            senderId = user1;
            receiverId = user2;

        } else {
            receiverId = user1;
            senderId = user2;

        }
        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(this);
//        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        messageAdapterList= new messageAdapter(this,new ArrayList<messageEntity>());
        mRecyclerView.setAdapter(messageAdapterList);


        sendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              final String message=etMessage.getText().toString();
              if (isEmpty(message)){
                  etMessage.setError("please enter message");
              }

              messageEntity messageEntity1= new messageEntity();
              messageEntity1.setBody(message);
              messageEntity1.setReceiveId(receiverId);

                Xmpp xmpp= Xmpp.getInstance();
                xmpp.sendMsg(message,receiverId);
                messageAdapterList.addList(messageEntity1);
                etMessage.setText("");



            }
        });


    }


    public void connectUser() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();
                config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                config.setServiceName("127.0.0.1");
                config.setPort(5222);
                config.setHost("192.168.1.8");
                mConnection = new XMPPTCPConnection(config.build());
                mConnection.addConnectionListener(ReceivedActivity.this);
                Log.d(TAG, "XMPPTCPConnectionUser: " + mConnection);
                try {
                    mConnection.connect();
                    Log.d(TAG, "XMPPTCPisAuthenticated: " + mConnection.isAuthenticated());
                    mConnection.login(userName, userPassword);

                    Log.d(TAG, "setConnection: " + mConnection.isAuthenticated());
                } catch (SmackException e) {
                    e.printStackTrace();
                    Log.d(TAG, "setConnectionSmackException: " + e.getMessage());
                } catch (IOException e) {
                    Log.d(TAG, "setConnectionIOException: " + e.getMessage());
                    e.printStackTrace();
                } catch (XMPPException e) {
                    Log.d(TAG, "setConnectionXMPPException: " + e.getMessage());
                    e.printStackTrace();
                }


            }
        }).start();

    }

    public void sendMessage() {
        sendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Chat chat = ChatManager.getInstanceFor(mConnection).createChat(receiverId+"@"+"127.0.0.1");
                final Message message = new Message();
                message.setBody(etMessage.getText().toString());
                message.setStanzaId(receiverId);
                message.setType(Message.Type.chat);
                try {
                    chat.sendMessage(message);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }

        });


        Log.d(TAG, "sendMessage: " + etMessage.getText().toString());
        Log.d(TAG, "sendMessageUserId: " + receiverId);


    }


    public void received() {
        XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();
        mConnection = new XMPPTCPConnection(config.build());
        if (mConnection.isAuthenticated()) {
            ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
            chatManager.addChatListener(new ChatManagerListener() {
                @Override
                public void chatCreated(Chat chat, boolean createdLocally) {
                    chat.addMessageListener(new ChatMessageListener() {
                        @Override
                        public void processMessage(Chat chat, Message message) {
                            Log.d(TAG, "ReceivedMsgUser: " + message.getBody());
                        }
                    });

                }
            });
        }

    }


    @Override
    public void connected(XMPPConnection connection) {
        Log.d(TAG, "userSendConnectedServer: " + connection.isConnected());

    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {


        Log.d(TAG, "userSendConnectedServerAuthenticated: " + connection.isAuthenticated());
        if (connection.isAuthenticated()) {
            ChatManager chatManager = ChatManager.getInstanceFor(connection);
            chatManager.addChatListener(
                    new ChatManagerListener() {
                        @Override
                        public void chatCreated(Chat chat, boolean createdLocally)
                        {
                            chat.addMessageListener(new ChatMessageListener()
                            {
                                @Override
                                public void processMessage(Chat chat, Message message) {
                                    Log.d(TAG, "ReceiveMessage: "+message.getBody());
                                }
                            });


                        }
                    });
        }

    }

    @Override
    public void connectionClosed() {

    }

    @Override
    public void connectionClosedOnError(Exception e) {

    }

    @Override
    public void reconnectionSuccessful() {

    }

    @Override
    public void reconnectingIn(int seconds) {

    }

    @Override
    public void reconnectionFailed(Exception e) {

    }

}
