package com.example.openfiredemo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements ConnectionListener {
    EditText userEt, passwordEt;
    Button loginButton;
    private static final String TAG = "loginUserOpenFire";
    String user;
    String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userEt = findViewById(R.id.user);
        passwordEt = findViewById(R.id.password);
        loginButton = findViewById(R.id.addUser);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 final Xmpp xmpp = Xmpp.getInstance();
                xmpp.login(userEt.getText().toString(),passwordEt.getText().toString(),listener);

//                setConnection();
            }
        });


    }

    onLoginListener listener = new onLoginListener() {
        @Override
        public void onSuccess() {
            Intent intent =new Intent(MainActivity.this,ReceivedActivity.class);
            intent.putExtra("user",userEt.getText().toString());
            intent.putExtra("pass",passwordEt.getText().toString());
            startActivity(intent);

            Log.d(TAG, "onSuccess: ");

        }

        @Override
        public void onFailure(String message) {
            Log.d(TAG, "onFailure: "+message);

        }
    };


    XMPPTCPConnection mConnection;


    public void setConnection() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                Xmpp xmpp= Xmpp.getInstance();



                XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();
                config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                config.setUsernameAndPassword(userEt.getText().toString(), passwordEt.getText().toString());
                config.setServiceName("127.0.0.1");
                config.setPort(5222);
                config.setHost("192.168.0.103");

                 mConnection = new XMPPTCPConnection(config.build());
                mConnection.addConnectionListener(MainActivity.this);

                try {
                    mConnection.connect();
                    mConnection.login();

                    Log.d(TAG, "setConnection: " + mConnection.isAuthenticated());
                } catch (SmackException e) {
                    e.printStackTrace();
                    Log.d(TAG, "setConnectionSmackException: " + e.getMessage());
                } catch (IOException e) {
                    Log.d(TAG, "setConnectionIOException: " + e.getMessage());
                    e.printStackTrace();
                } catch (XMPPException e) {
                    Log.d(TAG, "setConnectionXMPPException: " + e.getMessage());
                    e.printStackTrace();
                }


            }


        }).start();

    }


    @Override
    public void connected(XMPPConnection connection) {
        Log.d(TAG, "UserServerConnected: " + connection.isConnected());

    }

    @Override
    public void authenticated(final XMPPConnection connection, boolean resumed) {

        if (connection.isAuthenticated()) {
            mConnection.disconnect();
            mConnection = null;
            Intent intent = new Intent(MainActivity.this, ReceivedActivity.class);
            intent.putExtra("user", userEt.getText().toString());
            intent.putExtra("pass",passwordEt.getText().toString());
            startActivity(intent);


        }
        Log.d(TAG, "UserServerConnectedisAuthenticated: " + connection.isAuthenticated());

    }

    @Override
    public void connectionClosed() {

    }

    @Override
    public void connectionClosedOnError(Exception e) {

    }

    @Override
    public void reconnectionSuccessful() {

    }

    @Override
    public void reconnectingIn(int seconds) {

    }

    @Override
    public void reconnectionFailed(Exception e) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}

