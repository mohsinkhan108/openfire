package com.example.openfiredemo;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class messageAdapter extends RecyclerView.Adapter<messageAdapter.MyViewHolder> {
    private List<messageEntity> messageEntityList;
    private Context mContext;
    private static final String TAG = "messageAdapter";

    public messageAdapter(Context context, List<messageEntity> messageEntityList1) {
        mContext = context;
        this.messageEntityList= messageEntityList1;
    }




    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_message, parent, false);

        return new messageAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        messageEntity messageEntity=messageEntityList.get(position);
      holder.tvMsg.setText(messageEntity.getBody());
//      messageEntityList.add();
//      notifyDataSetChanged();
    }
    public void addList(messageEntity messageEntity){
        messageEntityList.add(0,messageEntity);
        notifyItemInserted(0);

        Log.d(TAG, "messageEntityListPosition: "+messageEntityList.indexOf(messageEntity));


    }

    @Override
    public int getItemCount() {
        return messageEntityList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMsg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMsg=itemView.findViewById(R.id.text_messageAdapter_message);

        }
    }

//    @Override
//    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
//    }
}
