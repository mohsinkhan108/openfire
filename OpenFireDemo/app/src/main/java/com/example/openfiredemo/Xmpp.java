package com.example.openfiredemo;


import android.net.sip.SipSession;
import android.util.Log;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;

public class Xmpp implements ConnectionListener {
    private static final String TAG = "configuration";
    private static Xmpp instance;
    private onLoginListener loginListener;
    XMPPTCPConnection connection;
    XMPPTCPConnectionConfiguration.Builder configuration;

    private Xmpp() {

        configuration = XMPPTCPConnectionConfiguration.builder();
        configuration.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        configuration.setServiceName("127.0.0.1");
        configuration.setPort(5222);
        configuration.setHost("192.168.0.103");
        connection = new XMPPTCPConnection(configuration.build());
        connection.addConnectionListener(Xmpp.this);

    }

    public static Xmpp getInstance() {

        if (instance == null) {
            instance = new Xmpp();
        }
        return instance;

    }

    public void login(final String userName, final String userPassword, onLoginListener listener) {
        loginListener = listener;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    connection.connect();
                    connection.login(userName, userPassword);
                } catch (XMPPException ex) {
                    ex.printStackTrace();
                    Log.d(TAG, "XMPPExceptionlogin: " + ex.getMessage());
                } catch (SmackException ex) {
                    ex.printStackTrace();
                    Log.d(TAG, "SmackExceptionlogin: " + ex.getMessage());
                } catch (IOException ex) {
                    ex.printStackTrace();
                    Log.d(TAG, "IOExceptionLogin : " + ex.getMessage());
                } catch (Exception e) {
                    loginListener.onFailure(e.getMessage());

                }
            }
        }).start();

    }

    public void sendMsg(String message,String userId) {
        Chat chat = ChatManager.getInstanceFor(connection).createChat(userId+ "@" + "127.0.0.1");
        final Message message1 = new Message();
        message1.setBody(message);
        message1.setStanzaId(userId);
        message1.setType(Message.Type.chat);
        try {
            chat.sendMessage(message1);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }

    }

    public  void receive() {

            ChatManager chatManager = ChatManager.getInstanceFor(connection);
            chatManager.addChatListener(
                    new ChatManagerListener() {
                        @Override
                        public void chatCreated(Chat chat, boolean createdLocally) {
                            chat.addMessageListener(new ChatMessageListener() {
                                @Override
                                public void processMessage(Chat chat, Message message) {
                                    Log.d(TAG, "ReceiveMessage: " + message.getBody());
                                }
                            });


                        }
                    });


    }

    @Override
    public void connected(XMPPConnection connection) {
        Log.d(TAG, "connected: " + connection.isConnected());

    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        if (connection.isAuthenticated()) {
            loginListener.onSuccess();
            ChatManager chatManager = ChatManager.getInstanceFor(connection);
            chatManager.addChatListener(
                    new ChatManagerListener() {
                        @Override
                        public void chatCreated(Chat chat, boolean createdLocally) {
                            chat.addMessageListener(new ChatMessageListener() {
                                @Override
                                public void processMessage(Chat chat, Message message) {
                                    Log.d(TAG, "ReceiveMessage: " + message.getBody());
                                }
                            });


                        }
                    });


        } else {
            loginListener.onFailure("failed");
        }


    }

    @Override
    public void connectionClosed() {

    }

    @Override
    public void connectionClosedOnError(Exception e) {

    }

    @Override
    public void reconnectionSuccessful() {

    }

    @Override
    public void reconnectingIn(int seconds) {

    }

    @Override
    public void reconnectionFailed(Exception e) {

    }
}

